#!/usr/bin/env python
# coding: utf-8
import sys
sys.path.append('..')
import numpy as np
from scipy import optimize

class Objective:
    def predict(self, x: np.ndarray, y: np.ndarray) -> np.ndarray:
        raise NotImplementedError()

class LinearUnivariate(Objective):
    def __init__(self):
        super().__init__()

    def predict(self, x: np.ndarray, y: np.ndarray) -> np.ndarray:
        ''' @todo Remove scipy call here, and write linear function from
            https://en.wikipedia.org/wiki/Segmented_regression#Segmented_linear_regression.2C_two_segments'''
        def _fit(x, a, b):
            return a * x + b
        slope, y_intercept = optimize.curve_fit(_fit, x, y)[0]
        y_hat =  slope * x + y_intercept
        return y_hat

class LinearMultivariate(Objective):
    def __init__(self): 
        super().__init__()
        self.beta_coef = None

    def get_beta_coef(self, x: np.ndarray, y: np.ndarray) -> np.ndarray:
        ''' @reference 
            https://www.stat.purdue.edu/~boli/stat512/lectures/topic3.pdf
            https://online.stat.psu.edu/stat462/node/132/'''
        n_obs = np.shape(x)[0]
        assert len(y) == n_obs

        # check for vector dimensions
        if len(np.shape(x)) < 2:
            x = x.reshape((n_obs, 1))

        # 1. Add ones column for the y_intercept
        ones = np.full((n_obs, 1), 1.)
        x = np.hstack((ones, x))

        # 2. Solve for b0, b1,..,bn
        beta_ = x.T.dot(x)
        # np.linalg.inv fails when values are too close
        # Add a very small value to the diagonal
        beta_[np.diag_indices_from(beta_)] += 10^-6
        beta_inv = np.linalg.inv(beta_)
        self.beta_coef = beta_inv.dot(x.T).dot(y)
        return self.beta_coef

    def predict(self, x: np.ndarray, y: np.ndarray) -> np.ndarray:
        assert len(y) == np.shape(x)[0]
        beta = self.get_beta_coef(x, y)
        y_hat = x.dot(beta[1:])
        y_hat += beta[0]
        return y_hat


def test_univariate():
    x = np.array([1, 2, 3, 4, 5, 6,  7,  8])
    y = np.array([20,30,12,18,20,21, 30, 32])
    obj = LinearUnivariate() 
    y_hat = obj.predict(x, y)
    print (x)
    print(y)
    print(y_hat)

def test_multivariate():
    x = np.array(
        [
            [4, 5, 7, 9, 10, 12, 15, 19],
            [4, 5, 7, 3, 10, 12, 15, 19],
            [4, 5, 7, 9, 10, 5, 15, 7]
        ], np.int32
    ).T
    y = np.array([20,30,12,18,20,21, 30, 32]).T
    obj = LinearMultivariate() 
    y_hat = obj.predict(x, y)
    print (y)
    print(y_hat)

if __name__ == '__main__':
    from utils.enums import RegressionType
    __regression_type = RegressionType.Multivariate

    if __regression_type == RegressionType.Univariate:
        test_univariate()
    else:
        test_multivariate()

