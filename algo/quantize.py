#!/usr/bin/env python
# coding: utf-8
import sys, os
sys.path.append('..')
from utils.enums import BorderType
import numpy as np
import pandas as pd
from typing import Callable

class Quantizer:
    def __init__(self, borderType: BorderType=BorderType.Quantile):
        self.border_type = borderType
    
    @staticmethod
    def show(cf: pd.DataFrame, features: np.ndarray, borderCount: int, title: str):
        assert len(cf.columns) == 2
        cf = cf.reset_index()
        cf['index'] = cf['index'] % borderCount
        cf = pd.pivot_table(cf, index='feature', columns='index', values='border')#.reset_index(drop=True)
        cf['feature'] = np.take(features, cf.index.values, axis=0)
        print('{} Quantized Data:\n{}'.format(title, cf.head(20)))

    def getBordersQuantile(self, featureValues, borderCount):
        qpoints = np.linspace(0, 1, borderCount+1)
        borders = np.quantile(featureValues, qpoints[1:borderCount+1])
        return borders

    def generateBorders(self, featureValues, borderCount):
        if self.border_type == BorderType.Quantile:
            return self.getBordersQuantile(featureValues, borderCount)
        else:
            raise NotImplementedError()

    def processFeatures(self, X, func):
        assert isinstance(X, np.ndarray)
        assert isinstance(func, Callable)
        if len(np.shape(X)) > 1:
            n_features = np.shape(X)[1]
            for i in range(n_features):
                func(i, X[:, i])
        else:
            # handle vector
            n_features = 1
            for i in range(n_features):
                func(i, X)

    def quantize(self, X, borderCount) -> pd.DataFrame:
        borders = None
        def _processFeature(featureIdx, featureValues):
            nonlocal borders
            assert isinstance(featureValues, np.ndarray)
            indexes = np.full((borderCount,), featureIdx)
            borders_ = self.generateBorders(featureValues, borderCount)
            this_borders = pd.DataFrame(data={0:indexes, 1:borders_})
            if borders is None:
                borders = this_borders
            else:
                borders = pd.concat([borders, this_borders], axis=0)

        self.processFeatures(X, _processFeature)
        return borders

