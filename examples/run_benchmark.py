#!/usr/bin/env python
# coding: utf-8
import sys, os
sys.path.append('..')
from utils.datasource import load_dataset
from utils.params import params
from utils.enums import ModelType
from algo.objective import LinearMultivariate
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_error
import numpy as np
import pandas as pd
from enum import Enum

__model_type = ModelType.Catboost
#__model_type = ModelType.MultivarLinReg

def multivar_lin_reg(x: np.ndarray, y: np.ndarray) -> np.ndarray:
    lr = LinearMultivariate()
    return lr.predict(x, y)

def catboost(x: np.ndarray, y: np.ndarray, features: list) -> np.ndarray:
    from catboost import CatBoost, Pool
    object_count, feature_count = np.shape(x)
    border_count = params['border_count']
    w = np.full((object_count,), 1.) 
    dx = Pool(x, y, weight=w, feature_names=features)
    dx.quantize(feature_border_type='Median', border_count=border_count)
    params_ = { 
        'iterations':1,
        'loss_function':'RMSE',
        'max_depth':2,
        'feature_border_type':'Median',
        'border_count': border_count,
        'random_strength':0,
        'bootstrap_type':'No',
    }   
    model = CatBoost(params_)
    model.fit(dx, logging_level='Info') 
    return model.predict(x) 

def run_benchmark():
    feature_count = params['feature_count']
    object_count = params['object_count']

    # Load dataset
    [X, y, features, w] = load_dataset()
    if feature_count > len(features):
        params['feature_count'] = len(features)
        feature_count = len(features)

    # Pre-proc dataset according to number of obs and features
    features = features[:feature_count]
    print("Features: {}".format(features))
    X = X[features]
    if object_count > np.shape(X)[0]:
        params['object_count'] = np.shape(X)[0]
        object_count = np.shape(X)[0]

    X = X.iloc[:object_count]
    y = y[:object_count]
    assert np.shape(X)[0] == np.shape(y)[0]
    print('Test Data: X: {} y: {}'.format(np.shape(X), np.shape(y)))
    X = np.asarray(X)

    # Partition data
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=.3, shuffle=False)

    # Run benchmark model
    if __model_type == ModelType.MultivarLinReg:
        prediction = multivar_lin_reg(X_train, y_train)
    elif __model_type == ModelType.Catboost:
        prediction = catboost(X_train, y_train, features)
    else:
        assert False,'Unknown model type'

    # Evaluate result
    print(np.shape(prediction))
    assert np.shape(prediction) == np.shape(y_train)
    rmse = mean_squared_error(prediction, y_train, squared=False)
    print('Predicted: {}'.format(prediction[:10]))
    print('Target: {}'.format(y_train[:10]))
    print('\nRMSE: {:.6f}'.format(rmse))


if __name__ == "__main__":
    data_path = params['data_path']
    if not os.path.exists(data_path): os.mkdir(data_path)
    run_benchmark()

