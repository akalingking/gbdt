#!/usr/bin/env python
# coding: utf-8
import sys, os
sys.path.append('..')
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_error
from algo.quantize import Quantizer
from utils.datasource import load_dataset
from utils.params import params
from utils.dataset import Dataset
from tree.treebuilder import TreeBuilder
from model.model import Model
import numpy as np
import pandas as pd

def main():
    feature_count = params['feature_count']
    object_count = params['object_count']
    border_count = params['border_count']

    # 0: Load dataset
    [X, y, features, w] = load_dataset()
    if feature_count > len(features):
        params['feature_count'] = len(features)
        feature_count = len(features)

    # 1. Pre-proc dataset according to amount of obs and features
    features = features[:feature_count]
    print("Features: {}".format(features))
    X = X[features]

    if object_count > np.shape(X)[0]:
        params['object_count'] = np.shape(X)[0]
        object_count = np.shape(X)[0]

    X = X.iloc[:object_count]
    y = y[:object_count]
    assert np.shape(X)[0] == np.shape(y)[0]
    print('Test Data: X: {} y: {}'.format(np.shape(X), np.shape(y)))
    X = np.asarray(X)
    w = np.full((object_count,), 1.) 

    # 2. Generate split points
    quantizer = Quantizer()
    borders = quantizer.quantize(X, borderCount=border_count)
    assert isinstance(borders, pd.DataFrame)
    assert np.shape(borders) == (border_count*feature_count,2)
    borders.columns = ['feature', 'border']
    Quantizer.show(borders, features, border_count, 'SplitPoints')

    # Convert model to [border_count, feature_count]
    borders_ = np.full((border_count, feature_count), np.nan)
    for i in range(feature_count):
        borders_[:, i] = borders[borders['feature']==i]['border']
   
    weights = np.full((object_count,), 1.)

    # 3. Partition data
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=.3, shuffle=False)
    w_train, w_test, _, _ = train_test_split(w, w, test_size=.3, shuffle=False)
    train_dataset = Dataset(X_train, y_train, w_train, borders_)
    test_dataset = Dataset(X_test, y_test, w_test, borders_)

    # 4. Create and train model
    model_ = Model()
    model_.train(train_dataset)

    # 5. Predict
    prediction = model_.predict(train_dataset.X)

    # 6. Evaluate result
    print(np.shape(prediction))
    assert np.shape(prediction) == np.shape(train_dataset.y)
    rmse = mean_squared_error(prediction, train_dataset.y, squared=False)
    print('Predicted: {}'.format(prediction[:10]))
    print('Target: {}'.format(train_dataset.y[:10]))
    print('RMSE: {:.6f}'.format(rmse))


if __name__ == "__main__":
    data_path = params['data_path']
    if not os.path.exists(data_path): os.mkdir(data_path)
    main()

