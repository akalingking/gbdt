#!/usr/bin/env python
# coding: utf-8
import sys, os
sys.path.append('..')
from utils.datasource import load_dataset
from algo.quantize import Quantizer
from utils.params import params
import numpy as np

def main():
    # 1. Configuration parameters
    feature_count = params['feature_count']
    object_count = params['object_count']
    border_count = params['border_count']

    # 2. Load dataset
    [X, y, features, w] = load_dataset()
    if feature_count > len(features):
        feature_count = len(features)

    features = features[:feature_count]
    print ("Features: {}".format(features))

    # 3. Subset data here
    X = X[features]
    if object_count > np.shape(X)[0]:
        object_count = np.shape(X)[0]
    X = X.iloc[:object_count]
    print('Test X: {}'.format(np.shape(X)))

    X = np.asarray(X)
    quantizer = Quantizer()
    borders = quantizer.quantize(X, borderCount=border_count)
    borders.columns=['feature', 'border']
    Quantizer.show(borders, features, border_count, 'Test')

if __name__ == "__main__":
    data_path = params['data_path']
    if not os.path.exists(data_path): os.mkdir(data_path)
    main()
