#!/usr/bin/env python
# coding: utf-8
import sys, os
sys.path.append('..')
from algo.quantize import Quantizer
from utils.datasource import load_dataset
from utils.params import params
from tree.treebuilder import TreeBuilder
import numpy as np
import pandas as pd

def run_tree():
    feature_count = params['feature_count']
    object_count = params['object_count']
    border_count = params['border_count']

    # 0: Load dataset
    [X, y, features, w] = load_dataset()
    if feature_count > len(features):
        params['feature_count'] = len(features)
        feature_count = len(features)

    # 1: Subset data here
    features = features[:feature_count]
    print("Features: {}".format(features))
    X = X[features]

    if object_count > np.shape(X)[0]:
        params['object_count'] = np.shape(X)[0]
        object_count = np.shape(X)[0]

    X = X.iloc[:object_count]
    y = y[:object_count]
    assert np.shape(X)[0] == np.shape(y)[0]
    print('Test Data: X: {} y: {}'.format(np.shape(X), np.shape(y)))
    X = np.asarray(X)
    w = np.full((object_count,), 1.) 
  
    quantizer = Quantizer()
    borders = quantizer.quantize(X, borderCount=border_count)
    assert isinstance(borders, pd.DataFrame)
    assert np.shape(borders) == (border_count*feature_count,2)
    borders.columns = ['feature', 'border']
    Quantizer.show(borders, features, border_count, 'SplitPoints')

    borders_ = np.full((border_count, feature_count), np.nan)
    for i in range(feature_count):
        borders_[:, i] = borders[borders['feature']==i]['border']
   
    # 2. Build Tree
    builder = TreeBuilder(
        maxDepth=params['max_depth'], 
        minSamplesPerLeaf=params['min_samples_per_leaf'], 
        maxBorderCount=border_count,
        weight=params['weight']
    )   

    tree = builder.buildTree(X, y, w, borders_)
    print('\nTree Structure (nodes: {}):'.format(len(tree.splits)))
    print(tree)

if __name__ == "__main__":
    data_path = params['data_path']
    if not os.path.exists(data_path): os.mkdir(data_path)
    run_tree()

