#!/usr/bin/env python
# coding: utf-8
''' @reference:
    [1] Greedy Function Approximation: A Gradient Boosting Machine. Jerome Friedman
        https://statweb.stanford.edu/~jhf/ftp/trebst.pdf'''
import sys
sys.path.append('..')
from utils.params import params
from tree.treebuilder import TreeBuilder
from tree.tree import Tree
from utils.metric import Metric
from utils.dataset import Dataset
from utils.enums import RegularizationType
import numpy as np

class Model:
    def __init__(self):
        self.best_iteration = -1
        self.iteration = params['iteration']
        self.feature_count = params['feature_count']
        self.object_count = params['object_count']
        self.learning_rate = params['learning_rate']
        self.max_train_delta = params['max_train_delta']
        if params['regularization'] == 'lasso':
            self.regularization = RegularizationType.Lasso
        elif params['regularization'] == 'ridge':
            self.regularization = RegularizationType.Ridge
        else:
            self.regularization = RegularizationType.Disabled
        self.reg_lambda = params['regularization_lambda']
        self.metric = Metric()

    def train(self, dataset: Dataset):
        ''' Section 1.2 Steepest Descent from [1]'''
        self.train_data = dataset
        self.object_count = dataset.object_count
        self.feature_count = dataset.feature_count

        print('Model.train object_count: {} feature_count: {}'.format(self.object_count, self.feature_count))

        self.gradients =  np.full((self.object_count, self.iteration), 0.)
        self.train_loss =  np.full((self.iteration, ), 0.)
        self.train_predict_values = np.full((self.object_count, self.iteration), 0.)
        self.theta = np.full((self.object_count, self.iteration + 1), 0.)

        self.trees = [TreeBuilder() for _ in range(self.iteration)]
        self.best_iteration = -1
        
        # Set initial values for theta_0 for gradient descent
        self.theta[:, 0].fill(1.0)
        for i in range(self.iteration):
            self.trees[i].buildTree(
                dataset.X,
                self.theta[:, i],
                dataset.w,
                dataset.borders,
            )

            assert self.trees[i].tree.leafNodeSize() >= 2, 'Invalid tree'
            #print(self.trees[i].tree)

            self.train_predict_values[:, i] = self.trees[i].predict(self.train_data.X)

            self.gradients[:, i] = self.metric.gradient(
                self.train_predict_values[:, i], 
                self.train_data.y, 
                #self.train_data.w
            )

            if self.regularization == RegularizationType.Lasso:
                self.theta[:, i+1] = self.theta[:, i] - self.learning_rate * self.gradients[:, i] * dataset.w 
                self.theta[:, i+1] += self.reg_lambda * dataset.w
            elif self.regularization == RegularizationType.Ridge:
                #self.theta[:, i+1] = self.theta[:, i] - self.learning_rate * self.gradients[:, i] * dataset.w 
                self.theta[:, i+1] = self.theta[:, i] - self.learning_rate * self.gradients[:, i] * dataset.w 
                self.theta[:, i+1] += self.reg_lambda * (dataset.w**2)
            else:
                self.theta[:, i+1] = self.theta[:, i] - self.learning_rate * self.gradients[:, i]

            self.train_loss[i] = self.metric.loss(self.theta[:, i+1], self.train_data.y)
            delta = self.train_loss[i] if i==0 else self.train_loss[i] - self.train_loss[i-1]
            print('iteration: {} loss: {:.6f} delta: {:.6f}'.format(i, self.train_loss[i], delta))
    
            # check for stop condition. Value must be <0 approaching a global minimum 
            if i > 0 and self.max_train_delta > 0. and delta > self.max_train_delta:
                break
        
        # Rank Scores
        best_train_loss = self.train_loss[0]
        self.best_iteration = 0
        fn = lambda lhs, rhs: (lhs < rhs)
        for j in range(i+1):
            delta = self.train_loss[j] if j==0 else self.train_loss[j]-self.train_loss[j-1]
            print('iteration: {} loss {:.6f} delta: {:.6f}'.format(j, self.train_loss[j], delta))
            if fn(self.train_loss[j], best_train_loss):
                best_train_loss = self.train_loss[j]
                self.best_iteration = j

        print('best iteration: {} loss: {:.6f}'.format(self.best_iteration, best_train_loss))

    def predict(self, x: np.ndarray) -> np.ndarray:
        assert len(self.trees) > 0
        return self.trees[self.best_iteration].predict(x)
                

