#!/usr/bin/env python
# coding: utf-8

class Node:
    def __init__(self, nodeId: int=-1):
        self.node_id = nodeId
        self.parent_node_id = -1
        self.sibling_node_id = -1
        self.left_child_node_id = -1
        self.right_child_node_id = -1
        self.split_info = None
        self.x = None
        self.y = None

class LinearNode(Node):
    def __init__(self, nodeId: int=-1):
        super().__init__(nodeId)
        self.gradient = None
        self.hessian = None
        self.objective = None
        
