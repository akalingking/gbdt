#!/usr/bin/env python
# coding: utf-8

class SplitInfo:
    def __init__(self, 
        featureIdx: int=-1, 
        border: float=0.,
        gain: float=0., 
        nodeId: int=-1,
        binIdx: int=-1
    ):
        self.feature_idx = featureIdx
        self.node_id = nodeId
        self.border = border
        self.gain = gain
        self.left_fitnode = None
        self.right_fitnode = None
