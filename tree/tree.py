#!/usr/bin/env python
# coding: utf-8
import sys
sys.path.append('..')
from .split import SplitInfo
from .node import LinearNode
from utils.metric import Metric 
from utils.dataset import DataPartition
from utils.params import params
from copy import deepcopy
import numpy as np
import pandas as pd

class _TreeBase:
    __last_id = -1
    def __init__(self, featureCount:int, featureIdx:int=None):
        Tree.__last_id += 1
        self.id = Tree.__last_id
        self.feature_count = featureCount
        self.nodes = {}
        self.feature_idx = featureIdx
        self.capacity = 0 
        self.is_leaf = np.zeros(0, dtype='bool')
        self.is_node = np.zeros(0, dtype='bool')
        self.leaf_object_counts = np.zeros(0)
        self.splits = []
        self._init()

    def _init(self):
        self._reallocIfNeeded(capacity=1)
        self.is_leaf[0] = True
        self.is_node[0] = True
        self.last_node_id = 0 
        root_node = LinearNode(0)
        self.nodes[0] = root_node

    def nodeSize(self):
        return len(self.nodes)

    def leafNodeSize(self):
        ret = 0
        for i in range(len(self.is_leaf)):
            ret += self.is_leaf[i]
        return ret
        
    def _growList(self, list, capacity, value=None):
        if len(list) >= capacity:
            return list
        else:
            return list + [value for _ in range(capacity - len(list))]

    def _reallocIfNeeded(self, capacity):
        if self.capacity <= capacity:
            self.is_leaf.resize(capacity)
            self.is_node.resize(capacity)
            self.leaf_object_counts.resize(capacity)
            self.splits = self._growList(self.splits, capacity)
            self.capacity = capacity

    def nodesAtLevel(self, level: int, nodeType: str='all') -> list:
        assert nodeType in ['all', 'node', 'leaf']
        result = []
        for node_id in range(2 ** (level - 1) - 1, min(2 ** level - 1, self.last_node_id + 1)):
            if nodeType == 'all':
                result.append(node_id)
            elif nodeType == 'node':
                if self.is_node[node_id]:
                    result.append(node_id)
            else:
                if self.is_leaf[node_id]:
                    result.append(node_id)
        return result

    def leftChild(self, nodeId: int) -> int:
        assert nodeId >= 0 and nodeId <= self.last_node_id
        return (nodeId + 1) * 2 - 1 

    def rightChild(self, nodeId: int) -> int:
        assert nodeId >= 0 and nodeId <= self.last_node_id
        return (nodeId + 1) * 2

    def isLeaf(self, nodeId: int):
        assert nodeId >= 0 and nodeId <= self.last_node_id
        return self.is_leaf[nodeId]

    def __str__(self):
        def _print(curNodeId, pad='', isLastLeafOnLevel=True):
            if curNodeId > self.last_node_id or not self.is_node[curNodeId]:
                return ''
            if self.is_leaf[curNodeId]:
                node_str = 'Node: {} (n={})'.format(
                    curNodeId,
                    int(self.leaf_object_counts[curNodeId])
                )
            else:
                node_str = 'Node: {}: [Feat: {} border: {:.3f} gain: {:.3f}] ? (n={})'.format(
                    curNodeId,
                    self.splits[curNodeId].feature_idx,
                    self.splits[curNodeId].border,
                    self.splits[curNodeId].gain,
                    int(self.leaf_object_counts[curNodeId])
                )
            result = pad + ('\'-- ' if isLastLeafOnLevel else '|-- ') + node_str + '\n'
            if isLastLeafOnLevel:
                new_pad = pad + '    '
            else:
                new_pad = pad + '|   '

            result += _print(self.leftChild(curNodeId), new_pad, False)
            result += _print(self.rightChild(curNodeId), new_pad, True)
            return result
        return _print(0)
    
    def splitNode(self, nodeId: int, splitInfo: SplitInfo):
        raise NotImplementedError()

    def predict(self, x: np.ndarray) -> np.ndarray:
        raise NotImplementedError()


class Tree(_TreeBase):
    def __init__(self, featureCount:int, featureIdx:int=None):
        super().__init__(featureCount, featureIdx)

    def splitNode(self, nodeId: int, splitInfo: SplitInfo):
        #print('Tree.splitNode: nodeId: {} last_node_id: {}'.format(nodeId, self.last_node_id)) 
        assert isinstance(nodeId, int)
        assert isinstance(splitInfo, SplitInfo)
        assert nodeId >= 0
        assert nodeId <= self.last_node_id
        assert splitInfo.feature_idx >= 0 and splitInfo.feature_idx < self.feature_count
        assert self.isLeaf(nodeId) == True

        left_child_id = self.leftChild(nodeId)
        right_child_id = self.rightChild(nodeId)

        if right_child_id >= self.capacity:
            self._reallocIfNeeded(2 * self.capacity + 1)

        self.splits[nodeId] = deepcopy(splitInfo)
        self.is_leaf[nodeId] = False
        self.is_node[left_child_id] = True
        self.is_node[right_child_id] = True
        self.is_leaf[left_child_id] = True
        self.is_leaf[right_child_id] = True

        # Set Node properties
        assert nodeId in self.nodes
        self.nodes[nodeId].is_leaf = False
        self.nodes[nodeId].left_child_id = left_child_id
        self.nodes[nodeId].right_child_id = right_child_id
        self.nodes[nodeId].split_info = deepcopy(splitInfo)
        
        self.nodes[left_child_id] = LinearNode(left_child_id)
        self.nodes[left_child_id].is_leaf = True
        self.nodes[left_child_id].parent_node_id = nodeId
        self.nodes[left_child_id].sibling_node_id = right_child_id
        self.nodes[left_child_id].fitnode = splitInfo.left_fitnode
        
        self.nodes[right_child_id] = LinearNode(right_child_id)
        self.nodes[right_child_id].is_leaf = True
        self.nodes[right_child_id].parent_node_id = nodeId
        self.nodes[right_child_id].sibling_node_id = left_child_id
        self.nodes[right_child_id].fitnode = splitInfo.right_fitnode
        
        self.last_node_id = max(self.last_node_id, right_child_id)

    def predict(self, x: np.ndarray) -> np.ndarray:
        print('Tree.predict id: {}'.format(self.id))
        predict = np.full((len(x),), 0.)
        leaf_node_ids = []
        learning_rate = params['learning_rate']
        # 1. Get all the leaf nodes
        for nodeid, node in self.nodes.items():
            if self.isLeaf(nodeid):
                leaf_node_ids.append(nodeid)

        # 2. Check dimensions
        total_num_samples = 0
        for nodeid in leaf_node_ids:
            total_num_samples += len(self.nodes[nodeid].x)
        assert total_num_samples == len(x)
        
        # 3. Construct prediction from Piece-Wise Linear Regression node results 
        i = 0
        for nodeid in leaf_node_ids:
            node = self.nodes[nodeid]
            object_count, _ = np.shape(node.x)
            #print('nodeid: {} beta: {}'.format(nodeid, node.beta_coef))
            for j in range(object_count):
                predict[i] = node.fitnode.predict(x[j, :])
                #print(node.y[j], ' -> ', predict[i])
                i += 1
        #print('total_num_samples ', total_num_samples)
        return predict

