#!/usr/bin/env python
# coding: utf-8
''' @reference:
    [1] Gradient Boosting w/ Piece-Wise Linear Regression Trees. Shi, Li, & Li
        https://arxiv.org/abs/1802.05640
'''
import sys
sys.path.append('../')
from .split import SplitInfo
from .tree import Tree
from algo.objective import LinearMultivariate
from utils.metric import Metric 
from utils.dataset import DataPartition
from utils.params import params
import numpy as np
import pandas as pd
from typing import Callable

class FitNode:
    def __init__(self):
        self.learning_rate = params['fitnode_learning_rate']
        self.lambda_ = params['fitnode_lambda']
        self.iteration = params['fitnode_iteration']
        self.min_delta = params['fitnode_min_delta']
        self.metric = Metric()
        self.objective = LinearMultivariate()
        self.best_iteration = -1
        self.best_beta_coef = None
    
    def predict(self, x, y=None, weight=None):
        prediction = None
        if y is not None:
            self._fit(x, y)
        prediction = self._predict(x)
        if y is not None:
            gradient = self.metric.gradient(prediction, y, weight)
            hessian = self.metric.hessian(prediction, y, weight)
            loss = self._calcLoss(x, gradient, hessian)
            return (prediction, loss)
        return prediction

    def _fit(self, x:np.ndarray, y:np.ndarray):
        ''' [1].4.2 Incremental Feature Selection and Half Additive Fitting
            Using item 2 method, fully corrective fitting'''
        iteration = self.iteration
        learning_rate = self.learning_rate
        lambda_ = self.lambda_

        object_count, feature_count = np.shape(x)
        gradients = np.full((object_count, iteration), 0.)
        loss = np.full((iteration, ), 0.)
        predict = np.full((object_count, iteration), 0.)
        theta = np.full((object_count, iteration + 1), 0.)
        weight = np.full((object_count), 1.)
        betas = []

        # set initial values for gradient descent
        theta[:, 0].fill(1.)
        for i in range(iteration):
            predict[:, i] = self.objective.predict(x, theta[:, i])
            beta = self.objective.beta_coef
            assert len(beta) == feature_count + 1
            betas.append(beta)
            gradients[:, i] = self.metric.gradient(predict[:, i], y)
            # Calculate thetas with Ridge Regularization
            theta[:, i+1] = theta[:, i] - learning_rate * gradients[:, i] * weight
            theta[:, i+1] += lambda_ * weight **2
            loss[i] = self.metric.loss(theta[:, i+1], y)
            delta = loss[i] if i==0 else loss[i-1] - loss[i]
            #print('FitNode._fit iteration: {} loss: {} delta: {}'.format(i, loss[i], delta))
            if i > 0 and delta < self.min_delta:
                break
            
        self.best_iteration = 0
        self.best_beta_coef = betas[0]
        for j in range(1, i):
            if loss[j] < loss[self.best_iteration]:
                self.best_iteration = j
                self.best_beta_coef = betas[j]

        #print('FitNode._fit iterations: {} best: {} loss: {:.6f}'.format(i, self.best_iteration, loss[self.best_iteration]))

    def _predict(self, x:np.ndarray) -> np.ndarray:
        assert self.best_beta_coef  is not None
        assert self.best_iteration is not None
        if len(np.shape(x)) > 1:
            object_count, feature_count = np.shape(x)
            assert feature_count == len(self.best_beta_coef) - 1
        else:
            # support row vector prediction
            assert len(x) == len(self.best_beta_coef) - 1
        y_hat = x.dot(self.best_beta_coef[1:])
        y_hat += self.best_beta_coef[0]
        return y_hat
        
    def _calcLoss(self, x:np.ndarray, gradient:np.ndarray, hessian:np.ndarray) -> np.ndarray:
        # Loss = -1/2 * g^T * X * ((X^T*H*X + lambda*I)^-1) * X^T*g  [1].Eq.(4)
        # where: H is a diagonal matrix from [1].3
        object_count = np.shape(x)[0]
        #assert object_count >= self.min_samples_per_leaf
        assert np.shape(gradient)[0] == object_count
        assert np.shape(hessian)[0] == object_count

        ones = np.full((object_count, 1), 1.)
        x_ = np.hstack((ones, x))
        hessian = hessian.reshape((object_count, 1))
        # convert hessian to square matrix
        hessian = np.diag(hessian[:, 0])
        beta = x_.T.dot(hessian).dot(x_ + self.lambda_)
        #beta += lambda_ 
        assert np.shape(beta)[0] == np.shape(beta)[1]
        # add small value to diagonal, to avoid error in taking inverse
        beta[np.diag_indices_from(beta)] += 10^-6
        beta = np.linalg.inv(beta)
        beta = beta.dot(x_.T).dot(gradient)
        loss = - 0.5 * gradient.T.dot(x_).dot(beta)
        return loss

class SplitEval:
    def __init__(self, minSamplesPerLeaf: int, metric: Metric): #, lossFn: Callable):
        self.min_samples_per_leaf = minSamplesPerLeaf
        self.metric = metric
        #self.loss_fn = lossFn
    
    def get_splits(self, 
        nodeId: int,
        featureIdx: int,
        X: np.ndarray, 
        y: np.ndarray, 
        borders: list, 
        parentLoss: float,
        splits: list
    ) -> int:
        x = X[:, featureIdx] if len(np.shape(X)) > 1 else X
        for border_idx in range(len(borders)):
            border = borders[border_idx]
            # Apply splitpoint c usig X_m
            left_row_indices = np.where(x <= border)
            left_object_count = np.shape(left_row_indices)[1]
            if left_object_count < self.min_samples_per_leaf:
                continue

            # From [1].4.2. Fully-Corrective Fitting
            left_x = X[left_row_indices]
            left_y = y[left_row_indices]
            left_weights = np.full((left_object_count,), 1.)
            left_fitnode = FitNode()
            _, left_loss = left_fitnode.predict(left_x, left_y, left_weights)
 
            right_row_indices = np.where(x > border)
            right_object_count = np.shape(right_row_indices)[1]

            if right_object_count < self.min_samples_per_leaf:
                continue

            right_x = X[right_row_indices]
            right_y = y[right_row_indices]
            right_fitnode = FitNode()
            right_weights = np.full((right_object_count,), 1.)
            _, right_loss = right_fitnode.predict(right_x, right_y, right_weights)
            
            gain = left_loss + right_loss - parentLoss
            print('SplitEval.get_splits loss_l: {:.6f} loss_r: {:.6f} loss_p: {:.6f} gain: {:.6f}'.format(
                left_loss, right_loss, parentLoss, gain))
            
            split = SplitInfo()
            split.feature_idx = featureIdx
            split.node_id = nodeId
            split.border = border
            split.gain = gain
            split.left_fitnode = left_fitnode
            split.right_fitnode = right_fitnode
            splits.append(split)
        return len(splits)
                       
class TreeBuilder:
    def __init__(self,
        maxDepth: int=3,
        minSamplesPerLeaf: int=4,
        maxBorderCount: int=10,
        weight: float=1.
    ):
        self.max_depth = maxDepth
        self.min_samples_per_leaf = minSamplesPerLeaf
        self.max_border_count = maxBorderCount
        self.metric = Metric()
        self.objective = LinearMultivariate()
        self.data_partition = DataPartition()
        self.borders = None
        self.dataset = None
        self.tree = None
        self.weight = weight

    def splitDataset(self, 
        X: np.ndarray, 
        y: np.ndarray, 
        featureIdx: int, 
        border: float
    ):
        #print('TreeBuilder.splitDataset border: {}'.format(border))
        if len(np.shape(X)) > 1:
            mask = X[:, featureIdx] <= border
        else:
            mask = X <= border
        return X[mask], X[~mask], y[mask], y[~mask]

    def applySplit(self, tree: Tree, splitInfo: SplitInfo):
        assert isinstance(tree,Tree)
        assert isinstance(splitInfo, SplitInfo)
        assert splitInfo.node_id >= 0

        node_id = splitInfo.node_id
        tree.splitNode(node_id, splitInfo)

        X, y = self.data_partition.getData(node_id)
        # Parent node is split according to feature_idx and split point
        X_left, X_right, y_left, y_right = self.splitDataset(X, y, splitInfo.feature_idx, splitInfo.border)

        left_child_id = tree.leftChild(node_id)
        right_child_id = tree.rightChild(node_id)

        self.data_partition.setData(left_child_id, X_left, y_left)
        self.data_partition.setData(right_child_id, X_right, y_right)
    
    def _getBestSplit(self, candidateSplits: list) -> SplitInfo:
        best_split = None
        if candidateSplits is not None and  len(candidateSplits) > 0:
            for i in range(len(candidateSplits)):
                #print ('Tree._getBestSplit select from candidates: {}'.format(i))
                assert candidateSplits[i] is not None
                if best_split is None:
                    #print('Tree._getBestSplit best split is none', candidate_splits[i].gain, candidate_splits[i].node_id)
                    if candidateSplits[i].node_id != -1:
                        best_split = candidateSplits[i]
                else:
                    #print('Tree._getBestSplit best current: {} cand: {}'.format(best_split.gain, candidate_splits[i].gain))
                    if candidateSplits[i].gain > best_split.gain:
                        best_split = candidateSplits[i]
        return best_split

    def getBestSplit(self, nodes: list, level: int) -> list:
        ''' From [1].Section 3. Gradient Boosting with PL Trees, Algorithm 1'''
        #print('Tree.getBestSplit node size: {}'.format(len(nodes)))
        assert self.borders is not None
        assert isinstance(self.borders, np.ndarray)
        assert isinstance(nodes, list)
        assert len(nodes) > 0 

        max_bin = params['max_bin']
        assert max_bin > 0 

        if len(np.shape(self.dataset[0])) > 1:
             _, feature_count = np.shape(self.dataset[0])
        else:
            feature_count = 1
        assert feature_count > 0 
        
        # 0. Find best split for each node in current level
        n_feature = 1
        best_splits = [SplitInfo() for _ in range(len(nodes))]
        # todo. remove split eval below and use class
        split_eval = SplitEval(self.min_samples_per_leaf, self.metric)#, self._calcLoss)
        for node_idx in range(len(nodes)):
            # Perform EvalSplit from [1]
            node_id = nodes[node_idx]

            X, y = self.data_partition.getData(node_id) # get raw dataset from root node
            assert X is not None and isinstance(X, np.ndarray)
            assert y is not None and isinstance(y, np.ndarray)
       
            parent_object_count = np.shape(X)[0]
            #print ('Tree.getBestSplit: processing nodeid: {} parent object_count: {}'.format(node_id, parent_object_count))
            if parent_object_count <= 0:
                print('TreeBuilder.getBestSplit: level: {} parent object_count empty for nodeid: {}'.format(level, node_id))
                assert False
                continue

            weights = np.full((parent_object_count,), self.weight) 
            _, parent_loss = FitNode().predict(X, y, weights)
            

            candidate_splits = []
            for feature_idx in range(feature_count):
                if len(np.shape(X)) > 1:
                    x = X[:, feature_idx]
                else:
                    x = X

                #print('Tree.getBestSplit processing nodeid: {} feature_idx: {} min: {} max: {}'.format(
                #    node_id, feature_idx, min(x), max(x)))

                if len(np.shape(self.borders)) > 1:
                    borders = self.borders[:, feature_idx]
                else:
                    borders = self.borders

                borders = borders[~np.isnan(borders)] 
                if len(borders) <= 0:
                    print ('TreeBuilder.getBestSplit: level: {} node_id: {} no borders for feature_idx: {}'.format(
                        level, node_id, feature_idx))
                    continue

                split_eval.get_splits(node_id, feature_idx, X, y, borders, parent_loss, candidate_splits)

            print('TreeBuilder.getBestSplit: level: {} node_id: {} select best from {} candidate splits'.format(
                level, node_id, len(candidate_splits)))

            best_split = self._getBestSplit(candidate_splits)
            if best_split is None:
                print('TreeBuilder.getBestSplit: level: {} fail to generate split for node: {}'.format(level, node_id))
                return None
            else:
                best_splits[node_idx] = best_split

        assert len(best_splits) == len(nodes)
        return best_splits

    def _buildTree(self, tree: Tree, level: int):
        nodes_on_current_level = tree.nodesAtLevel(level)

        #print('TreeBuilder._buildTree level: {} max_depth: {} nodeids: {}'.format(level, self.max_depth, nodes_on_current_level))

        for node_id in nodes_on_current_level:
            assert self.data_partition.hasData(node_id)
            _, y = self.data_partition.getData(node_id)
            tree.leaf_object_counts[node_id] = len(y)

        leaves_reached = False
        if self.max_depth is not None and level >= self.max_depth:
            print('TreeBuilder._buildTree stop condition, level: {} > max_depth: {}'.format(level, self.max_depth))
            leaves_reached = True

        best_layer_splits = None
        if not leaves_reached:
            best_layer_splits = self.getBestSplit(nodes_on_current_level, level)
            if best_layer_splits is not None:
                assert isinstance(best_layer_splits, list)
                assert len(best_layer_splits) > 0
                assert isinstance(best_layer_splits[0], SplitInfo)
                assert len(nodes_on_current_level) == len(best_layer_splits)

        if best_layer_splits is None or len(best_layer_splits) < len(nodes_on_current_level):
            print('TreeBuilder._buildTree stop condition, no more split')
            leaves_reached = True

        if leaves_reached:
            for node_id in nodes_on_current_level:
                x, y = self.data_partition.getData(node_id)
                node = tree.nodes[node_id]
                node.x, node.y = x, y
                # Add node regression parameters: b_s,0 + sum_(j=1)^m_s{ b_s,j }
                assert node.fitnode.objective.beta_coef is not None
                node.beta_coef = node.fitnode.objective.beta_coef
                node.beta_coef = node.beta_coef.reshape((len(node.beta_coef), 1))
                #print ('nodeid: {} beta: {}'.format(node_id, node.beta_coef))
        else:
            for node_id, split in zip(nodes_on_current_level, best_layer_splits):
                print('TreeBuilder._buildTree node_id: {} split node_id: {}'.format(node_id, split.node_id))
                self.applySplit(tree, split)
            if self.max_depth is not None and level < self.max_depth:
                self._buildTree(tree, level+1)

    def buildTree(self,
        X: np.ndarray,
        y: np.ndarray,
        w: np.ndarray,
        borders: np.ndarray,
        featureIdx: int=None
    ) -> Tree:
        assert np.shape(w) == np.shape(y)

        if featureIdx is None:
            if len(np.shape(borders)) > 1:
                assert np.shape(borders) == (self.max_border_count, np.shape(X)[1])
            else:
                assert np.shape(borders) == (self.max_border_count,)
            self.borders = borders
        else:
            if len(np.shape(borders)) > 1:
                self.borders = borders[:, featureIdx]
            else:
                self.borders = borders

        if featureIdx is None:
            if len(np.shape(X)) > 1:
                _, feature_count = np.shape(X)
            else:
                feature_count = 1
        else:
            feature_count = 1
        
        self.tree = Tree(feature_count, featureIdx)
        self.dataset = np.copy(X), np.copy(y)
        self.data_partition.setData(0, X, y)

        self._buildTree(self.tree, level=1)

        return self.tree

    def predict(self, x:np.ndarray) -> np.ndarray:
        return self.tree.predict(x)

