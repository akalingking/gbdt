#!/usr/bin/env python
# coding: utf-8
import numpy as np

class DataPartition:
    def __init__(self):
        self.partitions = {}

    def setData(self, nodeid: int, X: np.ndarray, y: np.ndarray):
        assert isinstance(X, np.ndarray)
        assert isinstance(y, np.ndarray)
        assert np.shape(X)[0] == np.shape(y)[0]
        self.partitions[nodeid] = (np.copy(X), np.copy(y))

    def getData(self, nodeId: int) -> tuple:
        assert self.hasData(nodeId)
        return self.partitions[nodeId]
    
    def hasData(self, nodeId: int) -> bool:
        return nodeId in self.partitions and nodeId <= len(self.partitions.keys())

class Dataset:
    def __init__(self, X, y, w, borders):
        self.X = X 
        self.y = y 
        self.w = w 
        self.borders = borders
        self.object_count, self.feature_count = np.shape(X)
        assert np.shape(y) == np.shape(w)
        assert np.shape(y)[0] == self.object_count

