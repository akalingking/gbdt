#!/usr/bin/env python
# coding: utf-8
import sys
sys.path.append('..')
from utils.params import params
from sklearn.datasets import load_boston
import numpy as np
import pandas as pd

__fname = 'boston_dataset.csv'

def load_dataset():
    
    data_path = params['data_path']
    data_ = None
    fname = data_path + __fname
    try:
       print("Loading data from {}".format(fname))
       data_ = pd.read_csv(fname, index_col=0)
    except:
       pass
    if data_ is None or data_.empty:
        print("Loading {} from sklearn..".format(fname))
        data = load_boston()
        features = data.feature_names.tolist()
        X = pd.DataFrame(data.data, columns=data.feature_names)
        y = np.asarray(data.target).reshape((len(data.target),))
        try:
            data_ = X.copy()
            data_['MEDV'] = data['target']
            data_.to_csv(fname)
        except:
            print("Write error to file: {}!".format(fname))
    else:
        X = data_
        y = np.asarray(data_['MEDV'])
        X.drop(['MEDV'], axis=1, inplace=True)
        features = X.columns.tolist()
    w = np.full((len(y)), 1.) 

    print ("X: {}, y: {}, features: {}, w: {}".format(
        np.shape(X), np.shape(y), np.shape(features), np.shape(w)))

    assert len(features) == np.shape(X)[1]
    assert np.shape(y)[0] == np.shape(X)[0]
    assert np.shape(y) == np.shape(w)
    return (X, y, features, w)

