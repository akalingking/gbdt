from enum import Enum

class RegularizationType(Enum):
    Disabled = 0
    Lasso = 1
    Ridge = 2

class BorderType(Enum):
    Quantile = 1
    Median = 2

class RegressionType(Enum):
    Univariate = 1
    Multivariate = 2

class ModelType(Enum):
    MultivarLinReg = 1
    Catboost = 2


