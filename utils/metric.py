#!/usr/bin/env python
# coding: utf-8
import numpy as np
'''
Loss function derivatives
f(x) = (x-y)^2
f'(x) = 2(x - y)
f''(x) = 2
'''

class Metric:
    def __init__(self):
        pass
     
    def gradient(self, 
        prediction: np.ndarray, 
        target: np.ndarray, 
        weight: np.ndarray=None
    ) -> np.ndarray:
        assert np.shape(prediction) == np.shape(target), print(np.shape(prediction), np.shape(target))
        object_size = np.shape(target)[0]
        if weight is not None:
            assert np.shape(target) == np.shape(weight)
        grad = np.full((object_size,), 0.) 
        for i in range(object_size):
            grad[i] = 2 * (prediction[i] - target[i])
            if weight is not None:
                grad[i] *= weight[i]
        return grad 

    def hessian(self, 
        prediction : np.ndarray,
        target : np.ndarray,
        weight: np.ndarray=None
    ) -> np.ndarray:
        if weight is not None:
            assert np.shape(target) == np.shape(weight)
        object_size = np.shape(target)[0]
        hess = np.full((object_size,), 0.) 
        for i in range(object_size):
            hess[i] = 2
            if weight is not None:
                hess[i] *= weight[i]
        
        return hess

    def loss(self, 
        prediction: np.ndarray, 
        target: np.ndarray, 
        weight: np.ndarray=None  
    ) -> float:
        assert np.shape(prediction) == np.shape(target)
        if weight is not None:
            assert np.shape(target) == np.shape(weight)
            assert np.shape(weight) == np.shape(prediction)
        object_size = np.shape(target)[0]
        loss = 0.
        if weight is not None:
            numer, denom = 0., 0.
            for i in range(object_size):
                numer += weight[i] * np.power(target[i] - prediction[i], 2.0)
                denom += weight[i]
            loss = np.sqrt(numer/denom)
        else:
            for i in range(object_size):
                loss += np.power(target[i] - prediction[i], 2.0)
            loss = np.sqrt(loss)
        assert np.isscalar(loss)
        return loss

