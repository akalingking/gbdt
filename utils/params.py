#!/usr/bin/env python
# coding: utf-8

params = { 
    'data_path': './data/',
    # Model parameters
    'feature_count': 10, 
    'object_count': 400,
    'max_train_delta': 5.,
    'regularization': 'ridge',
    'regularization_lambda': 3,
    'learning_rate': .001,
    # Tree parameters
    'border_count': 10, 
    'max_depth': 4,
    'min_samples_per_leaf': 2,
    'max_bin': 50,
    'iteration': 250,
    'weight': 1.,
    'fitnode_lambda': 3.,
    'fitnode_learning_rate': .001,
    'fitnode_iteration': 100,
    'fitnode_min_delta': .001,
}

